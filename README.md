### Test technique Onogone

#### Installation
- git clone git@bitbucket.org:Tomasz_Ng/test-onogone.git
- yarn install

#### Commands
- yarn start (Run development environment)
- yarn build (Build for deployment)
- yarn test (Run unit tests)

#### Features

- File upload
- Data rendering
- Pagination
- Translations (fr, en)
- Toastr notifications
- Error page handling
- Unit tests

#### Browser support

- Chrome
- Safari
- Edge
- Opera
