import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {toContainElement} from '@testing-library/jest-dom';
import thunk from 'redux-thunk';
import {browserHistory as history, store} from '../../../../store';
import Progress from '../../../../components/views/uploader/Progress';
import * as actions from '../../../../actions/uploaderActions';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

expect.extend({toContainElement});

describe('Progress', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should render progress page correctly and start upload', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = new Blob();

    const {container} = render(
      <Provider store={initialState}>
        <Progress handleFileReader={actions.handleFileReader} history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should redirect to selection page when no file uploaded', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = null;

    const {container} = render(
      <Provider store={initialState}>
        <Progress handleFileReader={actions.handleFileReader} history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });
});
