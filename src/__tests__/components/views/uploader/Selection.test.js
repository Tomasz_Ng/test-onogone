import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {toContainElement} from '@testing-library/jest-dom';
import {browserHistory as history, store} from '../../../../store';
import Selection from '../../../../components/views/uploader/Selection';

const mockStore = configureStore();

expect.extend({toContainElement});

describe('Selection', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should render selection page correctly', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = {};

    const {container} = render(
      <Provider store={initialState}>
        <Selection history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });
});
