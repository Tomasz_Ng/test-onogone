import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {toContainElement} from '@testing-library/jest-dom';
import {browserHistory as history, store} from '../../../../store';
import Complete from '../../../../components/views/uploader/Complete';

const mockStore = configureStore();

expect.extend({toContainElement});

describe('Complete', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should render complete page correctly', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = {};

    const {container} = render(
      <Provider store={initialState}>
        <Complete history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should redirect to selection page when no file uploaded', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = null;

    const {container} = render(
      <Provider store={initialState}>
        <Complete history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });
});
