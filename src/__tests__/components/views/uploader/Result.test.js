import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {toContainElement} from '@testing-library/jest-dom';
import {browserHistory as history, store} from '../../../../store';
import Result from '../../../../components/views/uploader/Result';

const mockStore = configureStore();

expect.extend({toContainElement});

describe('Result', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should render result page correctly', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = {};
    testStore.uploader.uploadData = ['a;b;c;d','e;f;g;h','i;j;k;l','m;n;o;p','a;b;c;d','e;f;g;h','i;j;k;l','m;n;o;p','a;b;c;d','e;f;g;h','i;j;k;l','m;n;o;p'];
    testStore.uploader.uploadDataColumns = [1,2,3,4,5];

    const {container} = render(
      <Provider store={initialState}>
        <Result history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should redirect to selection page when no file uploaded', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = null;

    const {container} = render(
      <Provider store={initialState}>
        <Result history={history}/>
      </Provider>);

    expect(container).toMatchSnapshot();
  });
});
