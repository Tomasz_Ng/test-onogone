import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {Router as RouterDom} from 'react-router-dom';
import {toContainElement} from '@testing-library/jest-dom';
import {browserHistory as history, store} from '../../store';
import Layout from '../../components/Layout';

const mockStore = configureStore();

expect.extend({toContainElement});

describe('Layout', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should display main content correctly', () => {
    const {container} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <Layout/>
        </RouterDom>
      </Provider>);
    const main = container.querySelector('main');

    expect(container).toContainElement(main);
    expect(main.children.length).toBeGreaterThan(1);
    expect(main).toMatchSnapshot();
  });

  it('should display toastr correctly', () => {
    const {container} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <Layout/>
        </RouterDom>
      </Provider>);
    const toastr = container.querySelector('.redux-toastr');

    expect(container).toContainElement(toastr);
    expect(toastr).toMatchSnapshot();
  });
});
