import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {Router as RouterDom} from 'react-router-dom';
import {Router, LocationDisplay} from '../../../components/router/Router';
import {browserHistory as history, store} from '../../../store';

const mockStore = configureStore();

describe('Router', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should match selection page', () => {
    history.push('/');

    const {container, getByTestId} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <LocationDisplay/>
        </RouterDom>
      </Provider>);

    expect(getByTestId('location-display')).toHaveTextContent('/');
    expect(container).toMatchSnapshot();
  });

  it('should match progress page', () => {
    history.push('/progress');

    const {container, getByTestId} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <LocationDisplay/>
        </RouterDom>
      </Provider>);

    expect(getByTestId('location-display')).toHaveTextContent('/progress');
    expect(container).toMatchSnapshot();
  });

  it('should match complete page', () => {
    history.push('/complete');

    const {container, getByTestId} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <LocationDisplay/>
        </RouterDom>
      </Provider>);

    expect(getByTestId('location-display')).toHaveTextContent('/complete');
    expect(container).toMatchSnapshot();
  });

  it('should match result page', () => {
    history.push('/result');

    const {container, getByTestId} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <LocationDisplay/>
        </RouterDom>
      </Provider>);

    expect(getByTestId('location-display')).toHaveTextContent('/result');
    expect(container).toMatchSnapshot();
  });

  it('should land on a 404 page', () => {
    history.push('/error');

    const {container, getByRole} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <Router/>
        </RouterDom>
      </Provider>);

    expect(getByRole('heading')).toHaveTextContent('404 error');
    expect(container).toMatchSnapshot();
  })
});
