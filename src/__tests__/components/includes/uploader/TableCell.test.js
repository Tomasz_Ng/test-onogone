import React from 'react';
import {render} from '@testing-library/react';
import TableCell from '../../../../components/includes/uploader/TableCell';

describe('TableCell', () => {
  it('should render table cells elements from an array data', () => {
    const cellsData = [1,2,3,4];
    const {container} = render(
      <table>
        <tbody>
          <tr>
            <TableCell data={cellsData}/>
          </tr>
        </tbody>
      </table>
    );
    const tableCells = container.querySelectorAll('td');

    expect(tableCells).toHaveLength(cellsData.length);
    expect(tableCells).toMatchSnapshot();
  });
});
