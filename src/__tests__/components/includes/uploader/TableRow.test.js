import React from 'react';
import {render} from '@testing-library/react';
import TableRow from '../../../../components/includes/uploader/TableRow';

describe('TableRow', () => {
  it('should render table row elements containing table cells from a string array data', () => {
    const rowsData = ['a;b;c;d','e;f;g;h','i;j;k;l','m;n;o;p'];
    const cellsData = rowsData.flatMap((row) => row.split(';'));
    const {container} = render(
      <table>
        <tbody>
          <TableRow data={rowsData}/>
        </tbody>
      </table>);
    const tableRows = container.querySelectorAll('tr');
    const tableCells = container.querySelectorAll('td');

    expect(tableRows).toHaveLength(rowsData.length);
    expect(tableCells).toHaveLength(cellsData.length);
    expect(tableRows).toMatchSnapshot();
  });
});
