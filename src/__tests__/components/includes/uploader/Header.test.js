import React from 'react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {render} from '@testing-library/react';
import {store} from '../../../../store';
import Header from '../../../../components/includes/uploader/Header';

const mockStore = configureStore();

describe('Header', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  it('should display properly', () => {
    const {container} = render(
      <Provider store={initialState}>
        <Header/>
      </Provider>);

    expect(container).toHaveTextContent('File selection');
    expect(container).toHaveTextContent('Upload progress');
    expect(container).toHaveTextContent('Upload complete');
    expect(container).toMatchSnapshot();
  });
});
