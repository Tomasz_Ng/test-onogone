import React from 'react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {render, fireEvent} from '@testing-library/react';
import thunk from 'redux-thunk';
import * as actions from '../../../../actions/uploaderActions';
import {browserHistory as history, store} from '../../../../store';
import Footer from '../../../../components/includes/uploader/Footer';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('Footer', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render with next button disabled on selection step when no file selected', () => {
    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(1);
    expect(buttons[0]).toHaveTextContent('Next');
    expect(buttons[0]).toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should render with next button enabled on selection step when a file is selected', () => {
    let testStore = initialState.getState();
    testStore.uploader.file = {};

    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(1);
    expect(buttons[0]).toHaveTextContent('Next');
    expect(buttons[0]).not.toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should render with next button disabled and previous button enabled on progress step when upload is in progress', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 1;
    testStore.uploader.file = {};
    testStore.uploader.uploadProgress = 50;

    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(2);
    expect(buttons[0]).toHaveTextContent('Previous');
    expect(buttons[0]).not.toHaveAttribute('disabled');
    expect(buttons[1]).toHaveTextContent('Next');
    expect(buttons[1]).toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should render with next and previous buttons enabled on progress step when upload is complete', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 1;
    testStore.uploader.file = {};
    testStore.uploader.uploadProgress = 100;

    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(2);
    expect(buttons[0]).toHaveTextContent('Previous');
    expect(buttons[0]).not.toHaveAttribute('disabled');
    expect(buttons[1]).toHaveTextContent('Next');
    expect(buttons[1]).not.toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should render with finish and previous buttons enabled on complete step', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 2;
    testStore.uploader.file = {};
    testStore.uploader.uploadProgress = 100;

    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(2);
    expect(buttons[0]).toHaveTextContent('Previous');
    expect(buttons[0]).not.toHaveAttribute('disabled');
    expect(buttons[1]).toHaveTextContent('Finish');
    expect(buttons[1]).not.toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should render with previous button enabled on result page', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 3;
    testStore.uploader.file = {};
    testStore.uploader.uploadProgress = 100;

    const {container} = render(
      <Provider store={initialState}>
        <Footer/>
      </Provider>);
    const footer = container.querySelector('footer');
    const buttons = container.querySelectorAll('button');

    expect(container).toContainElement(footer);
    expect(buttons).toHaveLength(1);
    expect(buttons[0]).toHaveTextContent('Previous');
    expect(buttons[0]).not.toHaveAttribute('disabled');
    expect(container).toMatchSnapshot();
  });

  it('should call handleNextStep action on next button click', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 0;
    testStore.uploader.file = {};

    const {getByText} = render(
      <Provider store={initialState}>
        <Footer handleNextStep={actions.handleNextStep} history={history}/>
      </Provider>);
    const nextButton = getByText('Next').closest('button');
    const expectedAction = {
      'type': 'HANDLE_NEXT_STEP',
      'step': 1
    };

    fireEvent.click(nextButton);
    const action = initialState.getActions()[0];

    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });

  it('should call handlePreviousStep action on previous button click', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 2;
    testStore.uploader.file = {};

    const {getByText} = render(
      <Provider store={initialState}>
        <Footer handlePreviousStep={actions.handlePreviousStep} history={history}/>
      </Provider>);
    const previousButton = getByText('Previous').closest('button');
    const expectedAction = {
      'type': 'HANDLE_PREVIOUS_STEP',
      'step': 1,
      'file': {}
    };

    fireEvent.click(previousButton);
    const action = initialState.getActions()[0];

    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });

  it('should call handlePreviousStep action on previous button click removing file on selection step', () => {
    let testStore = initialState.getState();
    testStore.uploader.step = 1;
    testStore.uploader.file = {};

    const {getByText} = render(
      <Provider store={initialState}>
        <Footer handlePreviousStep={actions.handlePreviousStep} history={history}/>
      </Provider>);
    const previousButton = getByText('Previous').closest('button');
    const expectedAction = {
      'type': 'HANDLE_PREVIOUS_STEP',
      'step': 0,
      'file': null
    };

    fireEvent.click(previousButton);
    const action = initialState.getActions()[0];

    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });
});
