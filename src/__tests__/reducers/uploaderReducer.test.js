import {uploaderReducer} from '../../reducers/uploaderReducer';
import * as types from '../../constants';

describe('uploader reducer', () => {
  it('should return the initial state', () => {
    expect(uploaderReducer(undefined, {})).toMatchSnapshot();
  });

  it('should handle HANDLE_PREVIOUS_STEP', () => {
    expect(uploaderReducer([], {
      type: types.HANDLE_PREVIOUS_STEP,
      file: null,
      step: 0
    })).toMatchSnapshot();
  });

  it('should handle HANDLE_NEXT_STEP', () => {
    expect(uploaderReducer([], {
      type: types.HANDLE_NEXT_STEP,
      step: 0
    })).toMatchSnapshot();
  });

  it('should handle FILE_SELECTION_SUCCESS', () => {
    expect(uploaderReducer([], {
      type: types.FILE_SELECTION_SUCCESS,
      file: {},
      size: 0
    })).toMatchSnapshot();
  });

  it('should handle FILE_SELECTION_ERROR', () => {
    expect(uploaderReducer([], {
      type: types.FILE_SELECTION_ERROR,
      error: '',
    })).toMatchSnapshot();
  });

  it('should handle FILE_READER_PROGRESS', () => {
    expect(uploaderReducer([], {
      type: types.FILE_READER_PROGRESS,
      progress: 0,
    })).toMatchSnapshot();
  });

  it('should handle FILE_READER_SUCCESS', () => {
    expect(uploaderReducer([], {
      type: types.FILE_READER_SUCCESS,
      data: [],
      columns: [],
      step: 0
    })).toMatchSnapshot();
  });

  it('should handle FILE_READER_ERROR', () => {
    expect(uploaderReducer([], {
      type: types.FILE_READER_ERROR,
      error: ''
    })).toMatchSnapshot();
  });
});
