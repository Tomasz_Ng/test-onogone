import {rootReducer} from '../../reducers/rootReducer';

describe('root reducer', () => {
  it('should return the initial state', () => {
    expect(rootReducer(undefined, {})).toMatchSnapshot();
  });
});
