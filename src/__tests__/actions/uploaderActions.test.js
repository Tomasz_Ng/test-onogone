import {browserHistory as history, store} from '../../store';
import * as actions from '../../actions/uploaderActions';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {toastr} from 'react-redux-toastr';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('uploaderActions', () => {
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create an action to handle file selection success', () => {
    const target = {
      files: [
        {
          name: 'file.csv',
          size: 10000000
        }
      ]
    };
    const expectedAction = {
      'type': 'FILE_SELECTION_SUCCESS',
      'file': target.files[0],
      'size': (target.files[0].size / 1000000).toFixed(3)
    }

    initialState.dispatch(actions.handleFileSelection({target}));
    const action = initialState.getActions()[0];

    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });

  it('should create an action to handle file selection error', () => {
    const target = {
      files: [
        {
          name: 'file.pdf',
          size: 10000000
        }
      ]
    };
    const expectedAction = {
      'type': 'FILE_SELECTION_ERROR',
      'error': 'Please select a csv file'
    }

    jest.mock('react-redux-toastr');
    jest.spyOn(toastr, 'error');

    initialState.dispatch(actions.handleFileSelection({target}));
    const action = initialState.getActions()[0];

    expect(action).toEqual(expectedAction);
    expect(toastr.error).toHaveBeenCalledTimes(1);
    expect(action).toMatchSnapshot();
  });

  it('should create an action to handle previous step', () => {
    const step = 2;
    const file = {
      name: 'file.csv',
      size: 10000000
    };
    const expectedAction = {
      'type': 'HANDLE_PREVIOUS_STEP',
      'step': 1,
      'file': file
    }

    jest.spyOn(history, 'push');

    initialState.dispatch(actions.handlePreviousStep(file, step, history));
    const action = initialState.getActions()[0];

    expect(history.push).toHaveBeenCalledTimes(1);
    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });

  it('should create an action to handle next step', () => {
    const step = 1;
    const file = {};
    const expectedAction = {
      'type': 'HANDLE_NEXT_STEP',
      'step': 2
    }

    jest.spyOn(history, 'push');

    initialState.dispatch(actions.handleNextStep(file, step, history));
    const action = initialState.getActions()[0];

    expect(history.push).toHaveBeenCalledTimes(1);
    expect(action).toEqual(expectedAction);
    expect(action).toMatchSnapshot();
  });

  it('should create an action to handle file reader', () => {
    const file = new Blob();
    const fileReader = window.FileReader.prototype;
    const readAsText = jest.spyOn(fileReader, 'readAsText');

    initialState.dispatch(actions.handleFileReader(file));
    const action = initialState.getActions();

    expect(readAsText).toBeCalledWith(file);
    expect(action).toMatchSnapshot();
  });
});
