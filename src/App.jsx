import React from 'react';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {store, browserHistory} from './store';
import Layout from './components/Layout';
import {GlobalStyles} from './theme/globalStyles';

const App = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <GlobalStyles/>
        <Layout/>
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
