import {en} from './messagesEn.js';
import {fr} from './messagesFr.js';

export const messages = {
  'en': Object.assign(en),
  'fr': Object.assign(fr)
};
