export const en = {
  'uploader.selection.title': 'File selection',
  'uploader.selection.formats': 'Required format : csv',
  'uploader.selection.size': 'Size : {size} Mo',
  'uploader.progress.title': 'Upload progress',
  'uploader.complete.title': 'Upload complete',
  'uploader.complete.rows': 'Total rows',
  'uploader.result.title': 'Result',
  'uploader.result.pagination.rows': 'Rows per page',
  'uploader.result.pagination.conjonction': ' of ',
  'uploader.button.next': 'Next',
  'uploader.button.previous': 'Previous',
  'uploader.button.finish': 'Finish',
  'error_404.title': '404 error',
  'error_404.message': 'Oops, the page you requested doesn\'t exist',
};

