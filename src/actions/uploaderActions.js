import {toastr} from 'react-redux-toastr';
import {
  HANDLE_PREVIOUS_STEP,
  HANDLE_NEXT_STEP,
  FILE_SELECTION_SUCCESS,
  FILE_SELECTION_ERROR,
  FILE_READER_PROGRESS,
  FILE_READER_SUCCESS,
  FILE_READER_ERROR
} from '../constants';

// Exports
export const handleFileSelection = ({target}) => {
  return (dispatch) => {
    const file = target.files[0];
    const extension = file.name.split('.')[1];
    const size = (file.size / 1000000).toFixed(3);

    if (extension === 'csv') {
      dispatch({
        type: FILE_SELECTION_SUCCESS,
        file: file,
        size: size
      });
    } else {
      let error = 'Please select a csv file';

      dispatch({
        type: FILE_SELECTION_ERROR,
        error: error
      });
      toastr.error('File format error', error, {className: 'error-toastr'});
    }
  };
};

export const handlePreviousStep = (file, step, history) => {
  return (dispatch) => {
    const previousStep = step - 1;
    const route = previousStep === 1 ? '/progress' : previousStep === 2 ? '/complete' : previousStep === 3 ? '/result' : '/';

    if (previousStep === 0) {
      file = null;
    }

    dispatch({
      type: HANDLE_PREVIOUS_STEP,
      file: file,
      step: previousStep
    });

    history.push(route);
  };
};

export const handleNextStep = (file, step, history) => {
  return (dispatch) => {
    const nextStep = step + 1;
    const route = nextStep === 1 ? '/progress' : nextStep === 2 ? '/complete' : nextStep === 3 ? '/result' : '/';

    dispatch({
      type: HANDLE_NEXT_STEP,
      step: nextStep
    });

    history.push(route);
  };
};

export const handleFileReader = (file) => {
  return (dispatch) => {
    const fileReader = new FileReader();

    fileReader.onprogress = (e) => { // Issue on firefox : fires only once
      dispatch({
        type: FILE_READER_PROGRESS,
        progress: (100 * e.loaded) / e.total
      });
    }

    fileReader.onload = (e) => {
      let data = e.target.result.split('\n');

      dispatch({
        type: FILE_READER_SUCCESS,
        data: data,
        columns: data.shift().split(';')
      });
    }

    fileReader.onerror = (e) => {
      dispatch({
        type: FILE_READER_ERROR,
        error: e.target.error
      });
      toastr.error('Upload interrupted', 'An error occured during upload', {className: 'error-toastr'});
    }

    return fileReader.readAsText(file);
  }
}
