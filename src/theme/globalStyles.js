import {createGlobalStyle} from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'Roboto-Light';
    font-weight: normal;
    font-style: normal;
    font-display: swap;
    unicode-range: U+000-5FF;
    src: local('Roboto-Light'),
    url('/fonts/Roboto-Light.ttf') format('truetype');
  }
  
  @font-face {
    font-family: 'Roboto-Regular';
    font-weight: normal;
    font-style: normal;
    font-display: swap;
    unicode-range: U+000-5FF;
    src: local('Roboto-Regular'),
    url('/fonts/Roboto-Regular.ttf') format('truetype');
  }
  
  * {
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-stroke: 0.45px rgba(0, 0, 0, 0.1);
  }
  
  body {
    font-family: 'Roboto-Light', sans-serif;
    font-size: 0.875rem;
  }
   
  #root {
    height: 100vh;
  }
  
  main {
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    overflow: auto;
    justify-content: flex-start;
  }
  
  h1 {
    margin-bottom: 15px;
    font-family: 'Roboto-Regular, sans-serif';
  }
  
  input[type=file] {
    padding: 0;
    height: 24px;
    background: aliceblue;
    margin-top: 10px;
    margin-bottom: 10px;
  }
  
  .MuiStepper-root {
    width: 100%;
  }
  
  .MuiLinearProgress-root {
    width: 300px; 
    height: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
  }
  
  .MuiTablePagination-root {
    .MuiToolbar-root {
      justify-content: center;
      
      .MuiTablePagination-spacer {
        display: none;
      }
    }
  }
`;
