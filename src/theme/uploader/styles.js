import styled from 'styled-components';

export const StyledHeader = styled.header`
  flex: 0.5;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const StyledSection = styled.section`
  flex: 0.5;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const StyledFooter = styled.footer`
  flex: 0.5;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;
