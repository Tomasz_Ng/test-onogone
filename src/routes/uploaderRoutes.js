import Selection from '../components/views/uploader/Selection';
import Progress from '../components/views/uploader/Progress';
import Complete from '../components/views/uploader/Complete';
import Result from '../components/views/uploader/Result';

export const uploaderRoutes = [
  {
    path: '/',
    name: 'selection',
    exact: true,
    component: Selection
  },
  {
    path: '/progress',
    name: 'progress',
    component: Progress
  },
  {
    path: '/complete',
    name: 'complete',
    component: Complete
  },
  {
    path: '/result',
    name: 'result',
    component: Result
  }
];
