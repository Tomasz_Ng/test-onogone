import React, {Fragment} from 'react';
import {Route, Switch, withRouter} from 'react-router';
import {uploaderRoutes} from '../../routes';
import Header from '../includes/uploader/Header';
import Footer from '../includes/uploader/Footer';
import Error from '../views/Error';

const LocationDisplay = withRouter(({location}) => (
  <div data-testid="location-display">{location.pathname}</div>
));

const Router = () => {
  return (
    <Switch>
      {uploaderRoutes.map((route, i) => (
        <Route
          key={i}
          name={route.name}
          path={route.path}
          exact={route.exact}
          state={route.state}
          children={(routeProps) => {
            return (
              <Fragment>
                <Header/>
                <route.component {...routeProps}/>
                <Footer {...routeProps}/>
              </Fragment>
            )
          }}
        />
      ))}

      <Route
        children={(routeProps) => (
          <Error {...routeProps}/>
        )}
      />
    </Switch>
  );
};

export {Router, LocationDisplay};
