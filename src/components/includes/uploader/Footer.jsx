import React from 'react';
import {Button} from '@material-ui/core';
import {connect} from 'react-redux';
import {handleNextStep, handlePreviousStep} from '../../../actions';
import {StyledFooter} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    step: state.uploader.step,
    file: state.uploader.file,
    uploadProgress: state.uploader.uploadProgress,
    messages: state.root.messages
  };
};

const mapDispatchToProps = {
  handleNextStep,
  handlePreviousStep
};

const Footer = (props) => {
  const renderPreviousButton = () => {
    if (props.step !== 0) {
      return (
        <Button
          onClick={() => props.handlePreviousStep(props.file, props.step, props.history)}>
            {props.messages['uploader.button.previous']}
        </Button>
      )
    }
  }

  const renderNextButton = () => {
    const hasNoFileSelected = props.step === 0 && !props.file;
    const isUploadProgressing = props.step === 1 && props.uploadProgress < 100;

    if (props.step !== 3) {
      return (
        <Button
          variant="contained"
          color="primary"
          onClick={() => props.handleNextStep(props.file, props.step, props.history)}
          disabled={hasNoFileSelected || isUploadProgressing}>
            {props.step !== 2 ? props.messages['uploader.button.next'] : props.messages['uploader.button.finish']}
        </Button>
      );
    }
  }

  return (
    <StyledFooter>
      {renderPreviousButton()}
      {renderNextButton()}
    </StyledFooter>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
