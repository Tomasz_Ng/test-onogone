import React, {Fragment} from 'react';
import {TableRow as Row} from '@material-ui/core';
import TableCell from './TableCell';

const TableRow = (props) => {
  return (
    <Fragment>
      {props.data.map((row, i) => {
        return (
          <Row key={i}>
            <TableCell data={row.split(';')}/>
          </Row>
        )
      })}
    </Fragment>
  );
};

export default TableRow;
