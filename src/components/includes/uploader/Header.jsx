import React from 'react';
import {Stepper, Step, StepLabel} from '@material-ui/core';
import {connect} from 'react-redux';
import {StyledHeader} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    step: state.uploader.step,
    messages: state.root.messages
  };
};

const Header = (props) => {
  return (
    <StyledHeader>
      <Stepper activeStep={props.step} alternativeLabel={true}>
        <Step>
          <StepLabel>{props.messages['uploader.selection.title']}</StepLabel>
        </Step>
        <Step>
          <StepLabel>{props.messages['uploader.progress.title']}</StepLabel>
        </Step>
        <Step>
          <StepLabel>{props.messages['uploader.complete.title']}</StepLabel>
        </Step>
      </Stepper>
    </StyledHeader>
  );
};

export default connect(mapStateToProps, null)(Header);
