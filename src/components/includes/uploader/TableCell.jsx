import React, {Fragment} from 'react';
import {TableCell as Cell} from '@material-ui/core';

const TableCell = (props) => {
  return (
    <Fragment>
      {props.data.map((cell, i) => {
        return <Cell key={i} align='center'>{cell}</Cell>
      })}
    </Fragment>
  );
};

export default TableCell;
