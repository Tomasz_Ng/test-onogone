import React from 'react';
import {Input} from '@material-ui/core';
import {connect} from 'react-redux';
import {handleFileSelection} from '../../../actions';
import {StyledSection} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    fileError: state.uploader.fileError,
    fileSize: state.uploader.fileSize,
    messages: state.root.messages
  };
};

const mapDispatchToProps = {
  handleFileSelection
};

const Selection = (props) => {
  return (
    <StyledSection>
      <h1>{props.messages['uploader.selection.title']}</h1>

      <p>{props.messages['uploader.selection.formats']}</p>

      <Input
        type="file"
        onChange={props.handleFileSelection}
        disableUnderline={true}
        inputProps={{
          'aria-label': props.messages['uploader.selection.title'],
          'accept': '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
        }}
      />

      <p>{props.messages['uploader.selection.size'].replace('{size}', props.fileSize)}</p>
    </StyledSection>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Selection);
