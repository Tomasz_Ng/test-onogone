import React, {useEffect, useState} from 'react';
import {Table, TableHead, TableBody, TableFooter, TableRow as Row, TablePagination} from '@material-ui/core';
import {connect} from 'react-redux';
import TableCell from '../../includes/uploader/TableCell';
import TableRow from '../../includes/uploader/TableRow';
import {StyledSection} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    uploadData: state.uploader.uploadData,
    uploadDataColumns: state.uploader.uploadDataColumns,
    file: state.uploader.file,
    messages: state.root.messages
  };
};

const Result = (props) => {
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState(0);

  const handleChangePage = (event, newPage) => {
    setOffset(newPage * rowsPerPage);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    if (!props.file) {
      props.history.push('/');
    }
  }, [props.file, props.history]);

  return (
    <StyledSection>
      <h1>{props.messages['uploader.result.title']}</h1>

      <Table stickyHeader={true}>
        <TableHead>
          <Row>
            <TableCell data={props.uploadDataColumns}/>
          </Row>
        </TableHead>

        <TableBody>
          <TableRow data={props.uploadData.slice(offset, offset + rowsPerPage)}/>
        </TableBody>

        <TableFooter>
          <Row>
            <TablePagination
              component={'td'}
              count={props.uploadData.length}
              rowsPerPageOptions={[5, 10, 25]}
              colSpan={props.uploadDataColumns.length}
              page={page}
              labelDisplayedRows={({from, to, count }) => {
                return from + '-' + to + props.messages['uploader.result.pagination.conjonction'] + count
              }}
              labelRowsPerPage={props.messages['uploader.result.pagination.rows']}
              SelectProps={{inputProps: {'aria-label': props.messages['uploader.result.pagination.rows']}}}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Row>
        </TableFooter>
      </Table>
    </StyledSection>
  );
};

export default connect(mapStateToProps, null)(Result);
