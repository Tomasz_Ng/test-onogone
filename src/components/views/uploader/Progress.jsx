import React, {useEffect} from 'react';
import {LinearProgress} from '@material-ui/core';
import {connect} from 'react-redux';
import {handleFileReader} from '../../../actions';
import {StyledSection} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    uploadProgress: state.uploader.uploadProgress,
    file: state.uploader.file,
    step: state.uploader.step,
    messages: state.root.messages
  };
};

const mapDispatchToProps = {
  handleFileReader
};

const Progress = (props) => {
  useEffect(() => {
    if (!props.file) {
      props.history.push('/');
    } else {
      props.handleFileReader(props.file);
    }
  }, []);

  return (
    <StyledSection>
      <h1>{props.messages['uploader.progress.title']}</h1>

      <LinearProgress variant="determinate" value={props.uploadProgress}/>
      <p>{props.uploadProgress} %</p>
    </StyledSection>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Progress);
