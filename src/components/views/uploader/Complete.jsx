import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {StyledSection} from '../../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    file: state.uploader.file,
    uploadData: state.uploader.uploadData,
    messages: state.root.messages
  };
};

const Complete = (props) => {
  useEffect(() => {
    if (!props.file) {
      props.history.push('/');
    }
  }, [props.file, props.history]);

  return (
    <StyledSection>
      <h1>{props.messages['uploader.complete.title']}</h1>
      <p>{props.messages['uploader.complete.rows']} : {props.uploadData.length}</p>
    </StyledSection>
  );
};

export default connect(mapStateToProps, null)(Complete);
