import React from 'react';
import {connect} from 'react-redux';
import {StyledSection} from '../../theme/uploader/styles';

const mapStateToProps = (state) => {
  return {
    messages: state.root.messages
  };
};

const Error = (props) => {
  return (
    <StyledSection>
      <h1>{props.messages['error_404.title']}</h1>
      <p>{props.messages['error_404.message']}</p>
    </StyledSection>
  );
};

export default connect(mapStateToProps, null)(Error);
