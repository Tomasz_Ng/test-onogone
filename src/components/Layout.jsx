import React, {Fragment} from 'react';
import ReduxToastr from 'react-redux-toastr';
import {Router} from './router/Router';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

const Layout = () => {
  return (
    <Fragment>
      <main>
        <Router/>
      </main>

      <ReduxToastr
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        timeOut={2000}/>
    </Fragment>
  );
};

export default Layout;
