import {messages} from '../messages';

const locale = window.navigator.language.substring(0, 2);

export const initialState = {
  root: {
    locale: locale,
    messages: messages[locale]
  }
};

export const rootReducer = (state = initialState, action) => {
  return state;
};
