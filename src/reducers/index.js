import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {reducer as toastrReducer} from 'react-redux-toastr';
import {rootReducer, initialState as rootState} from './rootReducer';
import {uploaderReducer, initialState as uploaderState} from './uploaderReducer';

export const initialState = Object.assign(rootState, uploaderState);

export const reducers = (browserHistory) => combineReducers({
    'root': rootReducer,
    'router': connectRouter(browserHistory),
    'uploader': uploaderReducer,
    'toastr': toastrReducer
});
