export const initialState = {
  uploader: {
    step: 0,
    file: null,
    fileSize: 0,
    fileError: null,
    uploadData: [],
    uploadDataColumns: [],
    uploadProgress: 0,
    uploadError: null
  }
};

export const uploaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'HANDLE_PREVIOUS_STEP':
      return {
        ...state,
        file: action.file,
        fileSize: 0,
        step: action.step
      };
    case 'HANDLE_NEXT_STEP':
      return {
        ...state,
        step: action.step
      };
    case 'FILE_SELECTION_SUCCESS':
      return {
        ...state,
        file: action.file,
        fileSize: action.size
      };
    case 'FILE_SELECTION_ERROR':
      return {
        ...state,
        fileError: action.error
      };
    case 'FILE_READER_PROGRESS':
      return {
        ...state,
        uploadProgress: action.progress
      };
    case 'FILE_READER_SUCCESS':
      return {
        ...state,
        uploadData: action.data,
        uploadDataColumns: action.columns
      };
    case 'FILE_READER_ERROR':
      return {
        ...state,
        uploadError: action.error
      };
    default:
      return state;
  }
};
